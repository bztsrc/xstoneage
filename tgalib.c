#include <stdio.h>
#include <stdlib.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>
#include <X11/keysym.h>
#include <X11/Intrinsic.h>
#include <zlib.h>
#include "xinit.h"

XImage *loadtga(gzFile gf)
{
    XImage *ximage;
    unsigned char header[18];
    unsigned char c,c2;
    int i,j,k,ibr,ibg, ibb;
    int picx,picy;
    long newpixval;
    char *data, *destptr;

    ximage=NULL;

    if(gf!=NULL){
#if DEBUG==TRUE
        printf("Loading picture...\n");
#endif

        //read tga file
        gzread(gf,&header,18);
        if (header[1]!=1 ||                     //color map type
           (header[2]!=1 && header[2]!=9) ||    //raw or RLE color mapped image
            header[7]!=24 ||                    //number of bits/color (8*3)
            header[16]!=8){                     //bits per pixel
                return(NULL);
        }
        picx=header[12]+header[13]*256;
        picy=header[14]+header[15]*256;

#if DEBUG==TRUE
        printf("File info: '");
#endif
        for(i=0;i<header[0];i++){
            gzread(gf,&c,1);
#if DEBUG==TRUE
            printf("%c",c);
#endif
        };	//skip file info
#if DEBUG==TRUE
        printf("'\n");
#endif

        ximage = XCreateImage(dis, vsl, DefaultDepth(dis,scr), ZPixmap, 0, NULL, picx, (picy+1), 8, picx * 4);
        data= (byte *) malloc(((picx * (picy+1))) * 4);
        if(ximage==NULL||data==NULL) {
            printf("Unable to create ximage\n");
            exit(2);
        }
        ximage->data= (char *)data;
        destptr= data;
        ximage->byte_order= LSBFirst;
        ximage->bits_per_pixel=32;
#if DEBUG==TRUE
        printf("XImage created\n");
#endif

        //read palette
        for(i=0;i<header[5]+header[6]*256;i++){
            j=header[3]+header[4]*256+i;
            j=j*3;
            gzread(gf,&pal[j+2],1);
            gzread(gf,&pal[j+1],1);
            gzread(gf,&pal[j+0],1);
        }
#if DEBUG==TRUE
        printf("Palette loaded\n");
#endif

        if(header[2]==1){
            //raw image
#if DEBUG==TRUE
            printf("Raw image\n");
#endif
            for(i=0;i<picy*2;i++){
                for(j=0;j<picx*2;j++){
                    gzread(gf,&c,1);
                    k=c*3;
                    newpixval= MkColorTGA(pal[k+0], pal[k+1], pal[k+2]);
                    valToMem(newpixval, destptr, 3);
                    destptr += 4;
                }
            }
        } else {
            // rle compressed
#if DEBUG==TRUE
            printf("RLE image\n");
#endif
            i=0; j=0;
            while(i<picy*picx){
                gzread(gf,&c2,1);
                if(c2 & 0x80){
                    //compressed package
                    c2&=0x7f; c2++;
                    gzread(gf,&c,1);
                    k=c*3;
                    newpixval= MkColorTGA(pal[k+0], pal[k+1], pal[k+2]);
                    while(c2>0){
                        valToMem(newpixval, destptr, 3);
                        destptr+=4;
                        i++;
                        c2--;
                    }
                } else {
                    //raw package
                    c2++;
                    while(c2>0){
                        gzread(gf,&c,1);
                        k=c*3;
                        newpixval= MkColorTGA(pal[k+0], pal[k+1], pal[k+2]);
                        valToMem(newpixval, destptr, 3);
                        destptr+=4;
                        i++;
                        c2--;
                    }
                }
            }
        }    
#if DEBUG==TRUE
        printf("File loaded, %dx%d data: %d bytes\n---\n",picx,picy,i);
#endif
    }
    return(ximage);
}

