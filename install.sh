#!/bin/bash

echo "xStoneAge Installer v0.0b"
echo ""

if [ `whoami` != root ]
then
    echo "Please run install script as root."
    exit
fi

echo -n "Creating directories... "
mkdir /var/games/xstoneage
chmod 777 /var/games/xstoneage
mkdir /usr/lib/xstoneage
chmod 777 /usr/lib/xstoneage
echo "OK."
echo -n "Copying files... "
cp xstoneage /usr/bin
chmod 755 /usr/bin/xstoneage
cp stoneage.pak /usr/lib/xstoneage
chmod 444 /usr/lib/xstoneage/stoneage.pak
cp xstoneage.6 /usr/share/man/man6
echo "OK."
echo "Installed."
