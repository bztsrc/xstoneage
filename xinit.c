#include <stdio.h>
#include <stdlib.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>
#include <X11/keysym.h>
#include <X11/Intrinsic.h>
#include "xinit.h"


/**
 * create an xwindow
 */
void initdisplay(char* file, int x, int y){
    int i;
    char bitmap_data=0;
    XColor black={0,0,0,0};
    Cursor cursor;
    Pixmap bitmap;
    Atom atom_motif_wm_hints;
    XSetWindowAttributes win_attr;
    XClassHint class_hints;

    dis = XOpenDisplay(NULL);
    if(dis==NULL){
        printf("Cannot connect to X server.\n");
        fflush(stdout);
        exit(1);
    }
    scr = DefaultScreen(dis);
    vsl = DefaultVisual(dis,scr);
    if(vsl->bits_per_rgb!=8){
        printf("Not a TrueColor visual. Please use DefaultDepth 24 mode.\n");
        fflush(stdout);
        exit(1);
    }
    /*
    XF86VidModeQueryVersion(dis, &vidModeMajorVersion, &vidModeMinorVersion);                                               
    XF86VidModeGetAllModeLines(dis, scr, &modeNum, &modes);                                                                 
    oldMode = *modes[0];                                                                                                    
    for (i=0;i<modeNum;i++) if(modes[i]->hdisplay==x && modes[i]->vdisplay==y) bestMode=i;                 
    XF86VidModeSwitchToMode(dis, scr, modes[bestMode]);                                                                     
    XF86VidModeSetViewPort(dis, scr, 0, 0);                                                                                 
    XFree(modes);                                                                                                           
    */
    colormap = DefaultColormap(dis, scr);
    red_shift=green_shift=blue_shift=24;
    for (i=vsl->red_mask;!(i&0x80000000);i<<=1) red_shift--;
    for (i=vsl->green_mask;!(i&0x80000000);i<<=1) green_shift--;
    for (i=vsl->blue_mask;!(i&0x80000000);i<<=1) blue_shift--;
    win = XCreateSimpleWindow(dis, RootWindow(dis, scr), 1, 1, x, y, 0, BlackPixel (dis, 0), BlackPixel(dis, 0));
    XMapWindow(dis, win);
    XRaiseWindow(dis, win);
    //XMoveWindow(dis, win, 0, 0);
    bitmap=XCreateBitmapFromData(dis, win, &bitmap_data, 1, 1);
    cursor=XCreatePixmapCursor(dis, bitmap, bitmap, &black, &black, 1, 1);
    //XDefineCursor(dis, win, cursor);
    sprintf(title,"%s",file); titlelen=strlen(title);
    XSetStandardProperties(dis,win,title,title,None,NULL,0,NULL);
    /*
    //hide decorations
    win_attr.override_redirect=False;                                                                                       
    XChangeWindowAttributes(dis,win,CWOverrideRedirect,&win_attr);                                                          
    class_hints.res_name=class_hints.res_class="FULLSCREEN";                                                          
    XSetWMProperties(dis,win,NULL,NULL,NULL,0,NULL,NULL,&class_hints);                                                      
    atom_motif_wm_hints=XInternAtom(dis,"_MOTIF_WM_HINTS",False);                                                           
    if(atom_motif_wm_hints!=0){                                                                                             
        motif_wm_hints.flags=MOTIF_WM_HINTS_DECORATIONS;                                                                    
        motif_wm_hints.decorations=0;                                                                                       
        XChangeProperty(dis,win,atom_motif_wm_hints,atom_motif_wm_hints,32,PropModeReplace,(unsigned char *)&motif_wm_hints,5);
    }                                                                                                                       
    */
    gc = XCreateGC(dis, win, 0, 0);
#if DEBUG==FALSE
    XSelectInput(dis, win, ExposureMask | KeyPressMask | KeyReleaseMask | ButtonPressMask | ButtonReleaseMask);
#else
    XSelectInput(dis, win, 0xFFFFFF-ResizeRedirectMask);
#endif
    XFlush(dis);
}

/**
 * destroy window
 */
void closedisplay()                                                                                                     
{                                                                                                                       
/*
    XF86VidModeSwitchToMode(dis, scr, &oldMode);                                                                        
    XF86VidModeSetViewPort(dis, scr, 0, 0);                                                                             
*/
    XDestroyWindow(dis,win);                                                                                            
    XCloseDisplay(dis);                                                                                                 
}                                                                                                                       

/**
 * Copy ximage subimage
 */
void CopyImage(int mode, XImage *bg, int x1, int y1, XImage* ximage, int x2, int y2, int w, int h)
{
    unsigned int dx,dy;
    unsigned long pix;
    for(dy=0;dy<h;dy++){
        for(dx=0;dx<w;dx++){
            pix=XGetPixel(ximage,dx+x2,dy+y2);
            if(mode==0 || pix!=0)
                XPutPixel(bg,dx+x1,dy+y1,pix);
        }
    }
}

/**
 * Create rgb colors
 */
unsigned long int MkColor(int r,int g,int b)
{
    if (red_shift>=0) r<<=red_shift; else r>>=-red_shift;
    if (green_shift>=0) g<<=green_shift; else g>>=-green_shift;
    if (blue_shift>=0) b<<=blue_shift; else b>>=-blue_shift;
    r&=vsl->red_mask;
    g&=vsl->green_mask;
    b&=vsl->blue_mask;
    return r+g+b;
}

unsigned long int MkColorTGA(int r,int g,int b)
{
    return (r&0xFF)+((g&0xFF)<<8)+((b&0xFF)<<16);
}

void DestroyImage(XImage *ximage){
    free(ximage->data);
    ximage->data=NULL;
    XDestroyImage(ximage);
}

void waitforevent(XImage *bg)
{
    report.type=None;
    while(report.type!=KeyPress && report.type!=ButtonRelease) {
        XNextEvent(dis, &report);
        //if (report.type==ButtonRelease) playsound("click.raw");
        if(report.type==Expose){
            XPutImage(dis, win, gc, bg,
                report.xexpose.x,report.xexpose.y,
                report.xexpose.x,report.xexpose.y,
                report.xexpose.width, report.xexpose.height);
            XSync(dis,FALSE);
        }
    }
}

/**
 * fade in our out an ximage
 */
void fadeimage(int fadetype, int x, int y, XImage *ximage){
    byte *data2;
    byte step, start, end;
    byte b;
    int i;
        
    data2=(byte *)malloc(ximage->bytes_per_line*ximage->height+12);
    bcopy(ximage->data,data2,ximage->bytes_per_line*ximage->height);

    //fade in
    if (fadetype==0 || fadetype==2){
        for(step=8;step>0;step--){
            for(i=0;i<ximage->bytes_per_line*ximage->height;i++){
                b=memToVal(data2+i,1); b=b/step;
                valToMem(b,ximage->data+i,1);
            }
            XPutImage(dis, win, gc, ximage, 0, 0, x, y, ximage->width, ximage->height);
            XSync(dis,FALSE);
            usleep(10000);
        }
    }
    //restore original image
    bcopy(data2,ximage->data,ximage->bytes_per_line*ximage->height);

    //wait for event
    if(fadetype==2){
        waitforevent(ximage);
    }

    //fade out
    if (fadetype==1 || fadetype==2 || fadetype==3){
        if(fadetype==3) end=4; else end=8;
        for(step=1;step!=end;step++){
            for(i=0;i<ximage->bytes_per_line*ximage->height;i++){
                b=memToVal(data2+i,1); b=b/step;
                valToMem(b,ximage->data+i,1);
            }
            XPutImage(dis, win, gc, ximage, 0, 0, x, y, ximage->width, ximage->height);
            XSync(dis,FALSE);
            usleep(10000);
        }
    }
    
    //restore original image
    if(fadetype!=3)
        bcopy(data2,ximage->data,ximage->bytes_per_line*ximage->height);
    free(data2);
}

/**
 * fade background and display a subtitle on it
 */
void subtitle(int x, int y, XImage *ximage, XImage *bg){
    byte *data2;
    byte step, start, end;
    byte b;
    int i;
        
    data2=(byte *)malloc(bg->bytes_per_line*bg->height+12);
    bcopy(ximage->data,data2,ximage->bytes_per_line*ximage->height);

    //fade in
    for(step=4;step>0;step--){
        for(i=0;i<ximage->bytes_per_line*ximage->height;i++){
            b=memToVal(data2+i,1); b=b/step;
            valToMem(b,ximage->data+i,1);
        }
        CopyImage(1,bg,x,y,ximage,0,0,ximage->width,ximage->height);
        XPutImage(dis, win, gc, bg, 0, 0, 0, 0, bg->width, bg->height);
        XSync(dis,FALSE);
        usleep(10000);
    }
    //restore original image
    bcopy(data2,ximage->data,ximage->bytes_per_line*ximage->height);

    //wait for event
    waitforevent(ximage);

    //fade out
    bcopy(bg->data,data2,bg->bytes_per_line*bg->height);
    for(step=1;step!=8;step++){
        for(i=0;i<bg->bytes_per_line*bg->height;i++){
            b=memToVal(data2+i,1); b=b/step;
            valToMem(b,bg->data+i,1);
        }
        XPutImage(dis, win, gc, bg, 0, 0, 0, 0, bg->width, bg->height);
        XSync(dis,FALSE);
        usleep(10000);
    }
    
    free(data2);
}

/**
 * mask out ximage
 */
void dopause(int x, int y, int legend, XImage *pause, XImage *bg){
    byte *data;
    XImage *tempimg;
    byte *data2;
    byte step, start, end;
    byte b;
    int i;
        
    data2=(byte *)malloc(bg->bytes_per_line*bg->height+12);
    bcopy(bg->data,data2,bg->bytes_per_line*bg->height);

    //Create Pause image
    tempimg = XCreateImage(dis, vsl, DefaultDepth(dis,scr), ZPixmap, 0, NULL, pause->width, pause->height, 8, pause->width * 4);
    data= (byte *) malloc(((pause->width * pause->height)) * 4);
    tempimg->data= (char *)data;
    tempimg->byte_order= LSBFirst;
    tempimg->bits_per_pixel=32;
    CopyImage(0,tempimg,0,0,bg,x,y,tempimg->width,tempimg->height);
    CopyImage(1,tempimg,0,0,pause,0,0,tempimg->width,tempimg->height);
    for(i=0;i<tempimg->bytes_per_line*tempimg->height;i++){
        b=memToVal(tempimg->data+i,1);
        if(b==0x010000 || b==0x000001) b=0;
        valToMem(b,tempimg->data+i,1);
    }

    //fade out bg
    for(step=1;step<8;step++){
        for(i=0;i<bg->bytes_per_line*(bg->height-legend);i++){
            b=memToVal(data2+i,1); if(step==7) b=1; else b=b/step;
            valToMem(b,bg->data+i,1);
        }
        CopyImage(1,bg,x,y,tempimg,0,0,tempimg->width,tempimg->height);
        XPutImage(dis, win, gc, bg, 0, 0, 0, 0, bg->width, bg->height-legend);
        XSync(dis,FALSE);
        usleep(10000);
    }

    //wait for event
    waitforevent(bg);

    //fade in bg
    for(step=8;step>0;step--){
        for(i=0;i<bg->bytes_per_line*(bg->height-legend);i++){
            b=memToVal(data2+i,1); b=b/step;
            valToMem(b,bg->data+i,1);
        }
        CopyImage(1,bg,x,y,tempimg,0,0,tempimg->width,tempimg->height);
        XPutImage(dis, win, gc, bg, 0, 0, 0, 0, bg->width, bg->height-legend);
        XSync(dis,FALSE);
        usleep(10000);
    }

    DestroyImage(tempimg);

    //copy back original screen
    bcopy(bg->data,data2,bg->bytes_per_line*bg->height);
    XPutImage(dis, win, gc, bg, 0, 0, 0, 0, bg->width, bg->height);
    XSync(dis,FALSE);

    free(data2);
}
