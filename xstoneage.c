/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

/*** xstoneage, stoneage for linux by bzt 2003 ***/

// Here goes the main stuff
// intro, menu, setup and the game itself
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/stat.h>
#include <time.h>
#include <dirent.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>
#include <X11/keysym.h>
#include <X11/Intrinsic.h>
#include <X11/cursorfont.h>
#include <zlib.h>
#include "xinit.h"

//load a tga file
XImage *loadtga(gzFile gf);

//level editor
void editor();

//intro, menu and setup images
XImage *stoneage;
XImage *menu;
XImage *light;
XImage *pass;
XImage *hiscore;
XImage *endseq;
XImage *fonts;
XImage *fonts2;
XImage *pauseimg;
XImage *ximage;

//images in game
XImage *bgr;
XImage *temp;

//items in game
XImage *tiles;
XImage *sidebars;
XImage *eyes;
XImage *dragon;

level levelmap;
unsigned char candles,lightgame;
unsigned int score,oldscore;

//other variables
int alt_pressed=FALSE;
int control_pressed=FALSE;
int mousebutton_pressed=FALSE;
int mousex,mousey,mdx,mdy;
int i,mode;
char cfgfile[512];
char scorefile[256];
int status,childpid,musicpid=0;
int timecnt, curr,selcurr,oldcurr,drag,anim,transposrting;
int olddrag,traveling,move, mtile, moving, movepix,mx,my,transporting;
int entering, leaving;
int redkey=0,greenkey=0;
int codeptr;
int rotateeye=0,rotcnt=0,drot=1;

hscoretype hiscores[9];
int mask[12]={3,0,1,4,1,5,9,2,6,5,3,5};
byte eyepos[220]={
    2,2,2,2,2,2,3,3,3,3,3,4,4,4,5,5,5,6,6,6,
    2,2,2,2,2,2,3,3,3,3,3,4,4,4,5,5,6,6,6,6,
    2,2,2,2,2,2,2,3,3,3,3,3,4,4,5,5,6,6,6,7,
    2,2,2,2,2,2,2,3,3,3,3,3,4,4,5,5,6,6,7,7,
    2,2,2,2,2,2,2,2,3,3,3,3,4,4,5,5,6,6,7,7,
    2,2,2,2,2,2,2,2,2,3,3,3,4,4,5,6,6,6,7,7,
    2,2,2,2,2,2,2,2,2,2,3,3,4,4,5,6,6,7,7,7,
    1,2,2,2,2,2,2,2,2,2,3,3,4,4,5,6,7,7,7,7,
    1,1,1,2,2,2,2,2,2,2,2,3,4,5,6,6,7,7,7,7,
    1,1,1,1,1,1,2,2,2,2,2,3,4,5,6,7,7,7,8,8
};

/**
 * start the game
 */
int startgame()
{
    fadeimage(FADEOUT,0,0,ximage);
    strcpy(levelmap.nextlink,"SRTLEV");
    score=0; candles=lightgame?10:5;
    dothegame();
    CopyImage(0, ximage, 0, 0, menu, 0, 0, menu->width, menu->height);
    fadeimage(FADEIN,0,0,ximage); XFlush(dis);
    mousex=1024;
}

/**
 * enter query password mode
 */
int getcode()
{
    fadeimage(FADEOUT,0,0,ximage);
    CopyImage(0, ximage, 0, 0, pass, 0, 0, pass->width, pass->height);
    fadeimage(FADEIN,0,0,ximage);
    mode=2;
    levelmap.nextlink[0]=0; codeptr=0;
}

/**
 * display hi scores
 */
int doscores()
{
    fadeimage(FADEOUT,0,0,ximage);
    displayhiscore();
    CopyImage(0, ximage, 0, 0, menu, 0, 0, menu->width, menu->height);
    fadeimage(FADEIN,0,0,ximage); XFlush(dis);
}

/**
 * toggle difficulty level
 */
int dodifficulty()
{
    CopyImage(0, ximage, 435, 100, menu, 435, 100, light->width, light->height);
    lightgame^=1;
    if(lightgame) CopyImage(1,ximage,435,100, light, 0,0, light->width,light->height);
    XPutImage(dis, win, gc, ximage, 435, 100, 435, 100, light->width, light->height);
    XSync(dis,FALSE);
}

/**
 * invoke level editor
 */
int doeditor()
{
    fadeimage(FADEOUT,0,0,ximage);
    XResizeWindow(dis,win,740,400);
    editor();
    XResizeWindow(dis,win,640,400);
    CopyImage(0, ximage, 0, 0, menu, 0, 0, menu->width, menu->height);
    fadeimage(FADEIN,0,0,ximage); XFlush(dis);
}

/**
 * program end, close display, store unsaved files etc.
 */
void byez(){
    fadeimage(FADEOUT,0,0,ximage);
    closedisplay();
    savescorefile();
#if DEBUG==TRUE
    printf("Exiting program.\n");
#else
    printf("\nDeveloped by bzt after the old DOS game, Stone Age\n"
             "Thanks to:\nMr.Break - idea, levels\n"
             "And, at last but not at least, my girlfriend's patience, who lacked me a lot\n"
             "while I was programming. And thanks for the coffees and food!\n");
#endif
    fflush(stdout);
    exit(0);
}

/**
 * load hi scores
 */
void loadscorefile(){
    int f; int i; int j;
    //default values
    for(i=0;i<9;i++) {
        hiscores[i].score=32<<(9-i);
        hiscores[i].level=9-i;
        strcpy((char*)&hiscores[i].name,i%3==0?"BZT":(i%3==1?"MR.BREAK":"SZISZI"));
    }
    //load
    f=open("/var/games/xstoneage/hiscores",O_RDONLY);
    if(f>2){
        for(i=0;i<9;i++){
            hiscores[i].score=0; read(f,&hiscores[i].score,3); hiscores[i].score^=(i+i*256+i*65536);
            hiscores[i].level=0; read(f,&hiscores[i].level,1); hiscores[i].level^=i;
            read(f,&hiscores[i].name,12);
            for(j=0;j<12;j++){
            hiscores[i].name[j]^=i+mask[j]+0x20;
            }
        }
        close(f);
    }
}

/**
 * save hi scores
 */
void savescorefile(void){
    int f; int i; int j;
#if DEBUG==TRUE
    printf("Saving hiscores.\n");
#endif
    mkdir("/var/games",RIGHTS(7,5,5));
    mkdir("/var/games/xstoneage",RIGHTS(7,5,5));
    f=open("/var/games/xstoneage/hiscores",O_WRONLY | O_CREAT,RIGHTS(6,4,4));
    if(f>2){
        for(i=0;i<9;i++){
            hiscores[i].score^=(i+i*256+i*65536);
            write(f,&hiscores[i].score,3);
            hiscores[i].level^=i;
            write(f,&hiscores[i].level,1);
            for(j=0;j<12;j++){
            hiscores[i].name[j]^=i+mask[j]+0x20;
            }
            write(f,&hiscores[i].name,12);
        }
        close(f);
    }
}

/**
 * Create an empty screen
 */
XImage *CreateEmptyImage()
{
    byte *data;
    XImage *ximage;
    ximage = XCreateImage(dis, vsl, 24, ZPixmap, 0, NULL, 640, 400, 8, 640 * 4);
    data= (byte *) malloc(((640 * 480)) * 4);
    bzero(data,(640 * 480) * 4);
    ximage->data= (char *)data;
    ximage->byte_order= MSBFirst;
    ximage->bits_per_pixel=24;
    return(ximage);
}

/**
 * create an image with scrolling test on it
 */
int drawscroll(XImage *ximage, XImage *bg,int x, int y, char *txt){
    int i,dx,dy;
    char c;
    dx=x; dy=y;
    for(i=0;i<strlen(txt);i++){
        if(txt[i]!='\n'){
            if(dy>=-32){
                c=txt[i];
                if(c!='_'){
                    if(c==' ') c='-';
                    if(c=='!') c='/';
                    CopyImage(0, ximage, dx, dy, bg, dx, dy,32,34);
                    CopyImage(1, ximage, dx, dy, fonts2, (c-',')*32, 0,32,32);
                }
                dx+=32;
                if(dx>=640){ dx=x; dy+=34;}
            }
        } else {
            dx=x;
            dy+=34;
        }
        if(dy>ximage->height) i=strlen(txt);
    }
}

/**
 * draw a number with pixmap font
 */
int drawnum(XImage *ximage,int x, int y, int num){
    char txt[16];
    int i;
    
    sprintf(txt,"%d",num);
    for(i=0;i<strlen(txt);i++){
        CopyImage(1, ximage, x-(strlen(txt)-i)*16, y, fonts, (txt[i]-'0')*16, 34,16,16);
    }
}

/**
 * draw a text with pixmap font
 */
int drawtext(XImage *ximage,int x, int y, char *txt){
    int i,dx,dy;
    char c;
    dx=x; dy=y;
    for(i=0;i<strlen(txt);i++){
        if(txt[i]!='\n'){
            c=txt[i];
            if(c!='_'){
                if(c==' ') c='-';
                if(c=='!') c='/';
                CopyImage(1, ximage, dx, dy, fonts2, (c-',')*32, 0,32,32);
            }
            dx+=32;
            if(dx>=640){ dx=x; dy+=34;}
        } else {
            dx=x;
            dy+=34;
        }
        if(dy>ximage->height) i=strlen(txt);
    }
}

/**
 * read next level
 */
int readlevel(){
    DIR *dh;
    struct dirent *de;
    unsigned char str[512];
    int path;
    int i,j,found;
    byte b;
    found=0;
#if DEBUG==TRUE
    printf("readlevel %s\n",levelmap.nextlink); fflush(stdout);
#endif
    if(strcmp(levelmap.nextlink,"ENDSEQ")==0){
        CopyImage(0,ximage,0,0,endseq,0,0,endseq->width,endseq->height);
        fadeimage(FADEIN,0,0,ximage);
        strcpy(str,"_<CONGRATULATIONS!>\n\n\n_<YOU HAVE MADE IT>\n\n_<YOU HAVE REACHED>\n___<THE EXIT !!!>");
    
        path=0; i=0;
        report.type=None;
        while(report.type!=KeyPress && report.type!=ButtonRelease){
            if(XPending(dis)) XNextEvent(dis, &report);
            //scroll text
            if(i<=300){
                drawscroll(ximage,endseq,0,ximage->height-i,(char *)&str);
                XPutImage(dis, win, gc, ximage, 0, 0, 0, 0, ximage->width, ximage->height);
                XSync(dis,FALSE);
            }
            if(i==350) CopyImage(0,ximage,0,0,endseq,0,0,endseq->width,endseq->height);
            //fade out background
            if(i>=350 && i<366){
                if(i%2==0){
                    for(j=0;j<ximage->bytes_per_line*ximage->height;j++){
                        b=memToVal(ximage->data+j,1); b=b/2;
                        valToMem(b,ximage->data+j,1);
                    }                                                                       
                    drawtext(ximage,0,100,(char*)&str);
                    XPutImage(dis, win, gc, ximage, 0, 0, 0, 0, ximage->width, ximage->height);
                    XSync(dis,FALSE);
                }
            }
            //fade congrat text
            if(i>=500 && i<516){
                if(i%2==0){
                    for(j=0;j<ximage->bytes_per_line*ximage->height;j++){
                        b=memToVal(ximage->data+j,1); b=b/2;
                        valToMem(b,ximage->data+j,1);
                    }                                                                       
                    XPutImage(dis, win, gc, ximage, 0, 0, 0, 0, ximage->width, ximage->height);
                    XSync(dis,FALSE);
                }
            }
            //message #1
            if(i==520){
                strcpy(str,"_<REMADE AFTER THE>_\n\n__< OLD DOS GAME >_\n\n_<BY ECLIPSE @1992>");
                drawtext(ximage,0,100,(char*)&str);
                fadeimage(FADEIN,0,0,ximage);
            }
            //message #2
            if(i==620){
                fadeimage(FADEOUT,0,0,ximage);
                for(j=0;j<ximage->bytes_per_line*ximage->height;j++) valToMem(0,ximage->data+j,1);
                strcpy(str,"__< IDEA:MR.BREAK=>\n__<=CODE:BZT >\n\n< LEVELS:MR.BREAK=>_______<@ BZT >");
                drawtext(ximage,0,100,(char*)&str);
                fadeimage(FADEIN,0,0,ximage);
            }
            //message #3
            if(i==720){
                fadeimage(FADEOUT,0,0,ximage);
                for(j=0;j<ximage->bytes_per_line*ximage->height;j++) valToMem(0,ximage->data+j,1);
                strcpy(str,"__<THANKS PLAYING>\n\n<= THIS FREE GAME @>\n\n__<=HOPE YOU HAVE>\n\n___< ENJOYED IT@>");
                drawtext(ximage,0,100,(char*)&str);
                fadeimage(FADEIN,0,0,ximage);
            }
            //message #4
            if(i==820){
                fadeimage(FADEOUT,0,0,ximage);
                for(j=0;j<ximage->bytes_per_line*ximage->height;j++) valToMem(0,ximage->data+j,1);
                strcpy(str,"_< LIVE FREE, USE >\n\n___<=  LINUX  @>\n\n__<FREE SOFTWARE>\n\n____<@FOREVER!=>");
                drawtext(ximage,0,100,(char*)&str);
                fadeimage(FADEIN,0,0,ximage);
            }
            //message #5
            if(i==920){
                fadeimage(FADEOUT,0,0,ximage);
                for(j=0;j<ximage->bytes_per_line*ximage->height;j++) valToMem(0,ximage->data+j,1);
                strcpy(str,"<STONEAGE FOR LINUX>\n\n__<  2003@ GPL  >");
                drawtext(ximage,0,100,(char*)&str);
                fadeimage(FADEIN,0,0,ximage);
            }
            //if a key pressed, stop end sequence
            i++; if(i>1020) report.type=KeyPress;
            usleep(i<350?10000:50000);
        }
        fadeimage(FADEOUT,0,0,ximage);
    } else {
        //look up level in pak
        for(i=0;i<numlevels;i++){
            if(strncmp(levelmap.nextlink,levels[i].code,6)==0){
                memcpy(&levelmap,&levels[i],sizeof(level));
                found=1; i=numlevels;
            }
        }
    
        //look up user defined level
        if(found==0){
            //search files
            strcpy(str,getenv("HOME"));
            strcat(str,"/.xstoneage/");
            path=strlen(str);
            dh=opendir(str);
            strcat(str,levelmap.nextlink);
            strcat(str,".map");
#if DEBUG==TRUE
            printf("File: %s\n",str); fflush(stdout);
#endif
            de=readdir(dh);
            while(de!=NULL){
                if(!strncmp(de->d_name,str + path,10)){
                    i=open(str,O_RDONLY);
                    read(i,&levelmap,sizeof(level));
                    close(i);
                    found=1;
                    break;
                }
                de=readdir(dh);
            };
            closedir(dh);
        }
        //level not found
        if(found==0){
            temp=CreateEmptyImage();
            sprintf(str,"\n\n<SORRY, BUT YOU DO>\n"
                            "NOT FIND THE CHAMBER__<WHICH NAME WAS:>\n\n"
                            "____<@ %s =>",
                        levelmap.nextlink);
            drawtext(temp,0,0,(char*)&str);
            fadeimage(FADEINOUT,0,0,temp);
            DestroyImage(temp);
            return(0);
        }
    }
    //if level found
    if(found){
        temp=CreateEmptyImage();
        drawtext(temp,0,0,"\n\n_<YOU ARE ABOUT TO>\n"
                               "= ENTER NEXT LEVEL.>\n\n\n"
                               "__<PRESS ANY KEY>\n"
                               "__<@IF YOU THINK>\n"
                               "<; YOU ARE READY =>");
        fadeimage(FADEINOUT,0,0,temp);
        DestroyImage(temp);
    }
    return(found);
}

/**
 * display the hi scroes
 */
void displayhiscore(){
    int i; char number[8];
    CopyImage(0,ximage,0,0,hiscore,0,0,hiscore->width,hiscore->height);
    for(i=0;i<9;i++){
        if(hiscores[i].score!=0){
            if(score==hiscores[i].score) {
                    drawtext(ximage,0,i*34+74,"                    ");
                    score=0;
            }
            sprintf(number,"%6d",hiscores[i].score);
            drawtext(ximage,10,i*34+74,(char*)&hiscores[i].name);
            drawtext(ximage,630-strlen(number)*32,i*34+74,(char*)&number);
        }
    }
    fadeimage(FADEINOUT,0,0,ximage);
}

/**
 * build scene
 */
void CreateScene(){
    int i,j,tmp;
    //scene
    for(i=0;i<ximage->height;i++) for(j=0;j<ximage->width;j++) XPutPixel(ximage,j,i,0);

    for(i=0;i<11;i++){
        for(j=0;j<20;j++){
            tmp=levelmap.map[i*20+j];
            if(tmp==0xff) {
                CopyImage(0, ximage, j*32, i*32, bgr, levelmap.tileset*640+j*32, levelmap.background*352+i*32, 32, 32);
            } else {
                CopyImage(0, ximage, j*32, i*32, tiles, (tmp/3)*32, (tmp%3)*32+levelmap.tileset*96,32,32);
            }
        }
    }

    CopyImage(0, ximage, 0, 355, sidebars, 0, levelmap.sidebar*45,640,45);
    drawnum(ximage, 95,370, timecnt);
    drawnum(ximage, 289,370, score);
    drawnum(ximage, 530,370, greenkey);
    drawnum(ximage, 630,370, redkey);

    if(eyepos[drag]<5) i=0; else i=26;
    j=42;
    if(eyepos[drag]==2 || eyepos[drag]==7) j=28;
    if(eyepos[drag]==3 || eyepos[drag]==6) j=14;
    if(eyepos[drag]==4 || eyepos[drag]==5) j=0;
    CopyImage(0, ximage, 420, 362, eyes, i, j,26,14);
}

/**
 * display screen from ximage buffer
 */
void exposehandler(){
    int dx,dy;
    //add dragon
    if(!moving && !traveling && !transporting && !entering && !leaving)
        CopyImage(1,ximage, (drag%20)*32, (drag/20)*32, dragon, 0, 0, 32, 32);

    //add sidebar and counters
    CopyImage(0, ximage, 0, 370, sidebars, 0, levelmap.sidebar*45+15,640,16);
    drawnum(ximage, 95,370, timecnt);
    drawnum(ximage, 289,370, score);
    drawnum(ximage, 530,370, greenkey);
    drawnum(ximage, 630,370, redkey);
    
    //add eye balls
    if(eyepos[drag]<5) dx=0; else dx=26;
    dy=42;
    if(eyepos[drag]==2 || eyepos[drag]==7) dy=28;
    if(eyepos[drag]==3 || eyepos[drag]==6) dy=14;
    if(eyepos[drag]==4 || eyepos[drag]==5) dy=0;
    CopyImage(0, ximage, 420, 362, eyes, dx, dy,26,14);
    
    //display
    XPutImage(dis, win, gc, ximage, 0, 0, 0, 0, ximage->width, ximage->height);
    XSync(dis,FALSE);
    fflush(stdout);

    //delay
    usleep(10000);
}

/**
 * start game choosen from menu
 */
void dothegame(){
    char duma[512],c;
    int i,j,exitgame=3,worth,dx,dy;
    time_t oldt,newt;
    redkey=0; greenkey=0;
    traveling=0; moving=0; transporting=0;
    movepix=0;
    //start background music
    musicpid=fork();
    if(musicpid==0){
        i=open("/usr/lib/xstoneage/music.mp3",O_RDONLY);
        if(i<3) {
            printf("Music file not found, no music.\n"); fflush(stdout);
            exit(1); 
        } else {
            close(i);
            system("mpg123 -q -Z /usr/lib/xstoneage/music.mp3");
        }
        exit(0);
    }
    //main loop for a level
    while(exitgame!=2){
        //failsafe
        if(exitgame==3) if(readlevel()==0) break;
    
        timecnt=levelmap.soltime*(lightgame+1);
        //calculate score for a level
        worth=0;
        for(i=0;i<220;i++){
            if(levelmap.map[i]==13){ drag=i; i=220;}
            if(levelmap.map[i]==11) worth+=2;	//disapear
            if(levelmap.map[i]==15) worth+=5;	//locks
            if(levelmap.map[i]==18) worth+=5;
            if(levelmap.map[i]==22) worth+=5;	//transport
            if(levelmap.map[i]==24) worth+=2;	//left
            if(levelmap.map[i]==25) worth+=2;	//right
            if(levelmap.map[i]==27) worth+=2;	//up
            if(levelmap.map[i]==28) worth+=2;	//down
            if(levelmap.map[i]==30) worth+=3;	//left-right
            if(levelmap.map[i]==31) worth+=3;	//up-down
            if(levelmap.map[i]==32) worth+=4;	//all directions
            if(levelmap.map[i]<=10) worth+=1;	//standard items
        }
    
        //build up scene
        CreateScene();
        fadeimage(FADEIN,0,0,ximage);
    
        anim=0; curr=0xff; selcurr=0xff; oldcurr=0xff;
        entering=1; movepix=8; exposehandler();
        exitgame=0;
            
        //main game loop on a given level
        while (!exitgame)  {
            //update eyes
            if(rotateeye){
                if(rotcnt<5) dx=0; else dx=26;
                dy=42;
                if(rotcnt==2 || rotcnt==7) dy=28;
                if(rotcnt==3 || rotcnt==6) dy=14;
                if(rotcnt==4 || rotcnt==5) dy=0;
                rotcnt+=drot; if(rotcnt>8) drot=-1; if(rotcnt<1) drot=1;
                CopyImage(0, ximage, 420, 362, eyes, dx, dy,26,14);
        
                XPutImage(dis, win, gc, ximage, 420, 362, 420, 362,26,14);
                timecnt=levelmap.soltime*(lightgame+1);
            }
            //get remaining time
            newt=time(NULL);
            if(newt!=oldt && !entering && !leaving) { 
                timecnt--; oldt=newt;
                if(!moving && !traveling && !transporting && !entering){
                    if(!moving && !traveling && !transporting && !entering && !leaving){
                        CopyImage(0, ximage,
                            (drag%20)*32, (drag/20)*32,
                            bgr,
                            levelmap.tileset*640+(drag%20)*32, levelmap.background*352+(drag/20)*32,
                            32, 32);
                        if(levelmap.map[drag]!=0xff)
                            CopyImage(1,ximage,
                            (drag%20)*32, (drag/20)*32,
                            tiles,
                            (levelmap.map[drag]/3)*32, (levelmap.map[drag]%3)*32+levelmap.tileset*96,
                            32, 32);
                        CopyImage(1,ximage, (drag%20)*32, (drag/20)*32, dragon, 0, 0, 32, 32);
                        XPutImage(dis, win, gc, ximage,
                            (drag%20)*32, (drag/20)*32, (drag%20)*32, (drag/20)*32,
                            33, 33);
                    }
                    CopyImage(0, ximage, 0, 370, sidebars, 0, levelmap.sidebar*45+15,96,16);
                    drawnum(ximage, 95,370, timecnt);
                    XPutImage(dis, win, gc, ximage, 0, 370, 0, 370, 96, 16);
                    XFlush(dis);
                }
            }
            //slow down entering and leaving animations
            if(entering || leaving) usleep(50000);
            //when run out of time
            if(timecnt==0) { exitgame=1; candles--; if(candles==0) exitgame=2; break; }
            usleep(10000);
            report.type=None;
            //when not in animation
            if(!moving && !traveling && !transporting && !entering && !leaving){
                if(XPending(dis)) XNextEvent(dis, &report);
                //handle events
                switch  (report.type) {

                case ButtonPress:
#if DEBUG==TRUE
                    printf("Mouse button pressed at %d %d.\n",report.xbutton.x,report.xbutton.y);
#endif
                    mousebutton_pressed=TRUE;
                    mousex=report.xbutton.x;
                    mousey=report.xbutton.y;
                    //cheat
                    if (mousex>=420 && mousey>=362 &&
                        mousex<=446 && mousey<=376 &&
                        alt_pressed==TRUE && control_pressed==TRUE) {
                            rotateeye=1;
                            timecnt=levelmap.soltime*(lightgame+1);
                            if(lightgame) i=10; else i=5;
                            candles=i;
                            score=0;
                            printf("Cheat enabled. New time: %d, candles: %d. Score 0.\n",timecnt,candles);
                            oldt=newt=time(NULL);
                            exposehandler();
                    }
                    break;

                case ButtonRelease:
#if DEBUG==TRUE
                    printf("Mouse button released at %d %d.\n",report.xbutton.x,report.xbutton.y);
#endif
                    mousebutton_pressed=FALSE;
                    if (mousex>=420 && mousey>=362 && mousex<=446 && mousey<=376) {
                        rotateeye=0;
                        timecnt=levelmap.soltime*(lightgame+1);
                        exposehandler();
                    }
            
                    if (mousex>=0 && mousey>=0 && mousex<=640 && mousey<=352) {
                        if(report.xbutton.button==3) curr=0xff; else {
                            curr=(mousey/32*20)+(mousex/32);
                            if( levelmap.map[curr]!=24 &&
                                levelmap.map[curr]!=25 &&
                                levelmap.map[curr]!=27 &&
                                levelmap.map[curr]!=28 &&
                                levelmap.map[curr]!=30 &&
                                levelmap.map[curr]!=31 &&
                                levelmap.map[curr]!=32) curr=0xff;
                        }
                        if(curr==drag) curr=0xff;
                        selcurr=oldcurr=0xff;
                        exposehandler();
                    }
                    break;	

                case ResizeRequest:   
#if DEBUG==TRUE
                    printf("I have been resized.\n");
#endif
                    XSetForeground(dis, gc, BlackPixel(dis,0)); XClearWindow(dis,win);
                    exposehandler();
                    break;

                case Expose:   
#if DEBUG==TRUE
                    printf("I have been exposed.\n");
#endif
                    exposehandler();
                    break;

                case KeyPress:
#if DEBUG==TRUE
                    printf("keypress: %X\n",XLookupKeysym(&report.xkey, 0));
#endif
                    if (XLookupKeysym(&report.xkey, 0) == XK_Alt_R || 
                        XLookupKeysym(&report.xkey, 0) == XK_Alt_L) {
                            alt_pressed=TRUE;
                    }
                    if (XLookupKeysym(&report.xkey, 0) == XK_Control_R || 
                        XLookupKeysym(&report.xkey, 0) == XK_Control_L) {
                            control_pressed=TRUE;
                    }
                    oldcurr=selcurr;
                    if (XLookupKeysym(&report.xkey, 0) == XK_Escape) {
                        exitgame=2;
                    }
                    if (XLookupKeysym(&report.xkey, 0) == XK_BackSpace) {
                        exitgame=1; candles--; if(candles==0) exitgame=2;
                    }
                    if (XLookupKeysym(&report.xkey, 0) == XK_Tab) {
                        CreateScene();
                        exposehandler();
                    }
                    if (XLookupKeysym(&report.xkey, 0) == XK_Return) {
                        curr=selcurr;
                        if( levelmap.map[curr]!=24 &&
                            levelmap.map[curr]!=25 &&
                            levelmap.map[curr]!=27 &&
                            levelmap.map[curr]!=28 &&
                            levelmap.map[curr]!=30 &&
                            levelmap.map[curr]!=31 &&
                            levelmap.map[curr]!=32) curr=0xff;
                        selcurr=oldcurr=0xff;
                        exposehandler();
                    }
                    if (XLookupKeysym(&report.xkey, 0) == XK_space) {
                        curr=0xff;
                        if(selcurr==0xff) selcurr=drag; else selcurr=0xff;
                        exposehandler();
                    }
                    if (XLookupKeysym(&report.xkey, 0) == XK_Left) {
                        if(selcurr!=0xff){
                            if(selcurr%20>0) selcurr--;
                        }else{
                            olddrag=drag;
                            if(curr!=0xff){
                                if(levelmap.map[curr]==24 || levelmap.map[curr]==31 || levelmap.map[curr]==32){
                                    move=curr; mtile=levelmap.map[move]; levelmap.map[move]=0xff; moving=1; }
                            } else {
                                if(drag%20>0 && levelmap.map[drag-1]==0xff && 
                                    (levelmap.map[drag]==24 || levelmap.map[drag]==31 || levelmap.map[drag]==32)){
                                        move=drag; mtile=levelmap.map[move]; levelmap.map[move]=0xff; moving=1;
                                }
                                if(drag%20>0 && levelmap.map[drag-1]>9 && 
                                    levelmap.map[drag-1]!=14 && levelmap.map[drag-1]!=17 &&
                                    levelmap.map[drag-1]!=20 && levelmap.map[drag-1]!=23 &&
                                    levelmap.map[drag-1]!=26 && levelmap.map[drag-1]!=29 &&
                                    levelmap.map[drag-1]!=0xff) {
                                        traveling=1; movepix=32; drag--;
                                }
                            }
                            if(!traveling && !moving && levelmap.map[drag]==22) {
                                olddrag=drag;
                                drag++; if(drag>220) drag=0;
                                while(levelmap.map[drag]!=22) {drag++; if(drag>220) drag=0;}
                                transporting=1; movepix=7;
                            }
                        }
                    }
                    if (XLookupKeysym(&report.xkey, 0) == XK_Right) {
                        if(selcurr!=0xff){
                            if(selcurr%20<19) selcurr++;
                        }else{
                            olddrag=drag;
                            if(curr!=0xff){
                                if(levelmap.map[curr]==25 || levelmap.map[curr]==31 || levelmap.map[curr]==32){
                                    move=curr; mtile=levelmap.map[move]; levelmap.map[move]=0xff; moving=2; }
                            } else {
                                if(drag%20<19 && levelmap.map[drag+1]==0xff &&
                                    (levelmap.map[drag]==25 || levelmap.map[drag]==31 || levelmap.map[drag]==32)){
                                        move=drag; mtile=levelmap.map[move]; levelmap.map[move]=0xff; moving=2;
                                }
                                if(drag%20<19 && levelmap.map[drag+1]>9 &&
                                    levelmap.map[drag+1]!=14 && levelmap.map[drag+1]!=17 &&
                                    levelmap.map[drag+1]!=20 && levelmap.map[drag+1]!=23 &&
                                    levelmap.map[drag+1]!=26 && levelmap.map[drag+1]!=29 &&
                                    levelmap.map[drag+1]!=0xff) {
                                        traveling=2; movepix=32; olddrag=drag; drag++;
                                }                    
                            }
                            if(!traveling && !moving && levelmap.map[drag]==22) {
                                olddrag=drag;
                                drag++; if(drag>220) drag=0;
                                while(levelmap.map[drag]!=22) {drag++; if(drag>220) drag=0; }
                                transporting=1; movepix=7;
                            }
                        }
                    }
                    if (XLookupKeysym(&report.xkey, 0) == XK_Up) {
                        if(selcurr!=0xff){
                            if(selcurr>=20) selcurr-=20;
                        }else{
                            olddrag=drag;
                            if(curr!=0xff){
                                if(levelmap.map[curr]==27 || levelmap.map[curr]==30 || levelmap.map[curr]==32){
                                    move=curr; mtile=levelmap.map[move]; levelmap.map[move]=0xff; moving=3; }
                            } else {
                                if(drag>=20 && levelmap.map[drag-20]==0xff && 
                                    (levelmap.map[drag]==27 || levelmap.map[drag]==30 || levelmap.map[drag]==32)){
                                        move=drag; mtile=levelmap.map[move]; levelmap.map[move]=0xff; moving=3;
                                }
                                if(drag>=20 && levelmap.map[drag-20]>9 &&
                                    levelmap.map[drag-20]!=14 && levelmap.map[drag-20]!=17 &&
                                    levelmap.map[drag-20]!=20 && levelmap.map[drag-20]!=23 &&
                                    levelmap.map[drag-20]!=26 && levelmap.map[drag-20]!=29 &&
                                    levelmap.map[drag-20]!=0xff) {
                                        traveling=3; movepix=32; olddrag=drag; drag-=20;
                                }
                            }
                            if(!traveling && !moving && levelmap.map[drag]==22) {
                                olddrag=drag;
                                drag++; if(drag>220) drag=0;
                                while(levelmap.map[drag]!=22) {drag++; if(drag>220) drag=0;}
                                transporting=1; movepix=7;
                            }
                        }
                    }
                    if (XLookupKeysym(&report.xkey, 0) == XK_Down) {
                        if(selcurr!=0xff){
                            if(selcurr<=200) selcurr+=20;
                        }else{
                            olddrag=drag;
                            if(curr!=0xff){
                                if(levelmap.map[curr]==28 || levelmap.map[curr]==30 || levelmap.map[curr]==32){
                                    move=curr; mtile=levelmap.map[move]; levelmap.map[move]=0xff; moving=4; }
                            } else {
                                if(drag<=200 && levelmap.map[drag+20]==0xff &&
                                    (levelmap.map[drag]==28 || levelmap.map[drag]==30 || levelmap.map[drag]==32)){
                                        move=drag; mtile=levelmap.map[move]; levelmap.map[move]=0xff; moving=4;
                                }
                                if(drag<=200 && levelmap.map[drag+20]>9 &&
                                    levelmap.map[drag+20]!=14 && levelmap.map[drag+20]!=17 &&
                                    levelmap.map[drag+20]!=20 && levelmap.map[drag+20]!=23 &&
                                    levelmap.map[drag+20]!=26 && levelmap.map[drag+20]!=29 &&
                                    levelmap.map[drag+20]!=0xff) {
                                        traveling=4; movepix=32; olddrag=drag; drag+=20;
                                }
                            }
                            if(!traveling && !moving && levelmap.map[drag]==22) {
                                olddrag=drag;
                                drag++; if(drag>220) drag=0;
                                while(levelmap.map[drag]!=22) {drag++; if(drag>220) drag=0; }
                                transporting=1; movepix=7;
                            }
                        }
                    }
                    if (XLookupKeysym(&report.xkey, 0) == XK_p) {
                        dopause(320-pauseimg->width/2,180-pauseimg->height/2,45,pauseimg,ximage);
                    }
                    break;

                case KeyRelease:
#if DEBUG==TRUE
                    printf("keyrelease: %X\n",XLookupKeysym(&report.xkey, 0));
#endif
                    if (XLookupKeysym(&report.xkey, 0) == XK_Alt_R ||
                        XLookupKeysym(&report.xkey, 0) == XK_Alt_L) {
                            alt_pressed=FALSE;
                    }
                    if (XLookupKeysym(&report.xkey, 0) == XK_Control_R ||
                        XLookupKeysym(&report.xkey, 0) == XK_Control_L) {
                            control_pressed=FALSE;
                    }
                    break;

                default:
                    break;
                }

                //selection cursor
                if(selcurr!=0xff){
                    if(oldcurr!=0xff && oldcurr!=selcurr) {
                        XPutImage(dis, win, gc, ximage, 
                            (oldcurr%20)*32, (oldcurr/20)*32,
                            (oldcurr%20)*32, (oldcurr/20)*32,
                            33, 33);
                        oldcurr=selcurr;
                    }
                    XSetForeground(dis, gc, MkColor(0,0,0));
                    XDrawRectangle(dis, win, gc, (selcurr%20)*32, (selcurr/20)*32, 32, 32);
                    XSetForeground(dis, gc, MkColor(255,255,255));
                    XDrawRectangle(dis, win, gc, (selcurr%20)*32+1, (selcurr/20)*32+1, 30, 30);
                    XFlush(dis);
                }
            }
        
            //moving to the next block
            if(moving && movepix<=0){
                if(moving==1){
                    if(move%20>0 && levelmap.map[move-1]==0xff) {
                        if(curr==move) curr--;
                        if(drag==move) drag--;
                        move--; movepix=32;
                    } else moving=0;
                }
                if(moving==2){
                    if(move%20<19 && levelmap.map[move+1]==0xff) {
                        if(curr==move) curr++;
                        if(drag==move) drag++;
                        move++; movepix=32;
                    } else moving=0;
                }
                if(moving==3){
                    if(move>=20 && levelmap.map[move-20]==0xff) {
                        if(curr==move) curr-=20;
                        if(drag==move) drag-=20;
                        move-=20; movepix=32;
                    } else moving=0;
                }
                if(moving==4){
                    if(move<=200 && levelmap.map[move+20]==0xff) {
                        if(curr==move) curr+=20;
                        if(drag==move) drag+=20;
                        move+=20; movepix=32;
                    } else moving=0;
                }
                if(moving==0){
                    movepix=0; levelmap.map[move]=mtile;
                    CopyImage(0,ximage,
                        (move%20)*32, (move/20)*32,
                        tiles,
                        (mtile/3)*32, (mtile%3)*32+levelmap.tileset*96,
                        32, 32);
                    exposehandler();
                }
            }
            if(moving){
                if(moving==1) {
                    mx=(move%20)*32+movepix; my=(move/20)*32;
                }
                if(moving==2) {
                    mx=(move%20)*32-movepix; my=(move/20)*32;
                }
                if(moving==3) {
                    mx=(move%20)*32; my=(move/20)*32+movepix;
                }
                if(moving==4) {
                    mx=(move%20)*32; my=(move/20)*32-movepix;
                }
                CopyImage(0,ximage, mx, my, tiles, (mtile/3)*32, (mtile%3)*32+levelmap.tileset*96,32, 32);
                if(drag==move) CopyImage(1,ximage, mx, my, dragon, 0, (moving%4)*32, 32, 32);
                exposehandler();
                CopyImage(0,ximage, mx, my, bgr, levelmap.tileset*640+mx, levelmap.background*352+my, 32, 32);
                movepix-=11;
            }
            if(traveling){
                if(levelmap.map[olddrag]==23) levelmap.map[olddrag]=0xff;
                if(levelmap.map[olddrag]==20) levelmap.map[olddrag]+=3;
                if(levelmap.map[olddrag]==17) levelmap.map[olddrag]+=3;
                if(levelmap.map[olddrag]==14) levelmap.map[olddrag]+=3;
                if(levelmap.map[olddrag]==11) levelmap.map[olddrag]+=3;
                if(levelmap.map[drag]==21) {timecnt+=100*(lightgame+1); levelmap.map[drag]=10;}
                if(levelmap.map[drag]==15)
                    if(greenkey>0) {
                        greenkey--; levelmap.map[drag]=10;
                    } else {
                        traveling=0; movepix=0; drag=olddrag;
                    }
                if(levelmap.map[drag]==16) {	greenkey++; levelmap.map[drag]=10; }
                if(levelmap.map[drag]==18)
                    if(redkey>0) {
                        redkey--; levelmap.map[drag]=10;
                    } else {
                        traveling=0; movepix=0; drag=olddrag;
                    }
                if(levelmap.map[drag]==19) {	redkey++; levelmap.map[drag]=10; }
                if(traveling==1) {
                    mx=(drag%20)*32+movepix; my=(drag/20)*32;
                }
                if(traveling==2) {
                    mx=(drag%20)*32-movepix; my=(drag/20)*32;
                }
                if(traveling==3) {
                    mx=(drag%20)*32; my=(drag/20)*32+movepix;
                }
                if(traveling==4) {
                    mx=(drag%20)*32; my=(drag/20)*32-movepix;
                }
                CopyImage(0,ximage,
                    (drag%20)*32, (drag/20)*32,
                    tiles,
                    (levelmap.map[drag]/3)*32, (levelmap.map[drag]%3)*32+levelmap.tileset*96,
                    32, 32);
                CopyImage(0, ximage,
                    (olddrag%20)*32, (olddrag/20)*32,
                    bgr,
                    levelmap.tileset*640+(olddrag%20)*32, levelmap.background*352+(olddrag/20)*32,
                    32, 32);
                if(levelmap.map[olddrag]!=0xff)
                    CopyImage(1,ximage,
                        (olddrag%20)*32, (olddrag/20)*32,
                        tiles,
                        (levelmap.map[olddrag]/3)*32, (levelmap.map[olddrag]%3)*32+levelmap.tileset*96,
                        32, 32);
                CopyImage(1,ximage, mx, my, dragon, (32-movepix)*4, (traveling%4)*32, 32, 32);
                exposehandler();
                movepix-=8;
            }
            if(traveling && movepix<0) {
                traveling=0; movepix=0;
                CopyImage(0,ximage,
                    (drag%20)*32, (drag/20)*32,
                    tiles,
                    (levelmap.map[drag]/3)*32, (levelmap.map[drag]%3)*32+levelmap.tileset*96,
                    32, 32);
                CopyImage(1,ximage, (drag%20)*32, (drag/20)*32, dragon, 0, 0, 32, 32);
                if(levelmap.map[drag]==22) {
                    olddrag=drag;
                    drag++; if(drag>220) drag=0;
                    while(levelmap.map[drag]!=22) {drag++; if(drag>220) drag=0; }
                    transporting=1; movepix=7;
                }
                if(levelmap.map[drag]==12) if(!leaving){
                    leaving=1; movepix=4;
                }
            }
            //moving or travelling on a stone
            if(transporting){
                if(movepix>3){
                    //disapear
                    CopyImage(0,ximage,
                        (olddrag%20)*32, (olddrag/20)*32,
                        tiles,
                        (levelmap.map[olddrag]/3)*32, (levelmap.map[olddrag]%3)*32+levelmap.tileset*96,
                        32, 32);
                    CopyImage(1, ximage, (olddrag%20)*32, (olddrag/20)*32, dragon, (7-movepix)*32, 128, 32, 32);
                } else {
                    //apear
                    if(movepix==3)
                        CopyImage(0,ximage, 
                            (olddrag%20)*32, (olddrag/20)*32,
                            tiles,
                            (levelmap.map[olddrag]/3)*32, (levelmap.map[olddrag]%3)*32+levelmap.tileset*96,
                            32, 32);
                    CopyImage(1, ximage, (drag%20)*32, (drag/20)*32, dragon, movepix*32, 128, 32, 32);
                }
                exposehandler();
                movepix--;
                if(movepix<0){ transporting=0; exposehandler();}
            }
            //entering the scene
            if(entering){
                for(i=0;i<220;i++) if(levelmap.map[i]==12 || levelmap.map[i]==13){
                    CopyImage(0, ximage,
                        (i%20)*32, (i/20)*32,
                        tiles,
                        (levelmap.map[i]/3)*32, (levelmap.map[i]%3)*32+levelmap.tileset*96,
                        32, 32);
                }
                if(movepix>=0){
                    if(movepix<5) CopyImage(1, ximage, (drag%20)*32, (drag/20)*32, dragon, (4-movepix)*32, 160, 32, 32);
                    for(i=0;i<220;i++) if(levelmap.map[i]==12){
                        CopyImage(0, ximage,
                            (i%20)*32, (i/20)*32,
                            tiles,
                            (levelmap.map[i]/3)*32, (levelmap.map[i]%3)*32+levelmap.tileset*96,
                            32, 32);
                        CopyImage(1, ximage,
                            (i%20)*32, (i/20)*32,
                            dragon,
                            (4-(movepix%4))*32, 224,
                            32, 32);
                    }
                }
                exposehandler();
                movepix--;
                if(movepix<0){
                    entering=0;
                    for(i=0;i<220;i++)
                        if(levelmap.map[i]==12 || levelmap.map[i]==13){
                            CopyImage(0, ximage,
                                (i%20)*32, (i/20)*32,
                                tiles,
                                (levelmap.map[i]/3)*32, (levelmap.map[i]%3)*32+levelmap.tileset*96,
                                32, 32);
                    }
                    timecnt=levelmap.soltime*(lightgame+1);
                    oldt=newt=time(NULL);
                    exposehandler();
                }
            }
            //leaving the scene
            if(leaving){
                CopyImage(0, ximage,
                    (drag%20)*32, (drag/20)*32,
                    tiles,
                    (levelmap.map[drag]/3)*32, (levelmap.map[drag]%3)*32+levelmap.tileset*96,
                    32, 32);
                if(movepix>0)
                    CopyImage(1, ximage, (drag%20)*32, (drag/20)*32, dragon, (4-movepix)*32, 192, 32, 32);
                exposehandler();
                movepix--;
                if(movepix<0){ leaving=0; exitgame=3; }
            }
        }
        fadeimage(3,0,0,ximage);
    
        temp=CreateEmptyImage();
            
        //empty keyboard buffer
        while(XPending(dis)) XNextEvent(dis, &report);
    
        //run out of time
        if(exitgame==1){
            sprintf(duma,"\n<A STRONG BLOW AND>\n"
                           "ALL AROUND YOU FADES<TO BLACK. LUCKY AS>"
                           "_EVER, YOU FIND YOUR<WAY BACK TO WHERE>\n"
                           "__<YOU STARTED !>\n\n"
                           "_<NOW, YOU HAVE %d>\n"
                           "__<CANDLES LEFT.>",candles);
            drawtext(temp,0,0,(char*)duma);
            subtitle(0,0,temp,ximage);
            for(i=0;i<numlevels;i++){
                if(strncmp(levelmap.code,levels[i].code,6)==0){
                    memcpy(&levelmap,&levels[i],sizeof(level));
                    i=numlevels;
                }
            }
        }
        //game over
        if(exitgame==2){
            drawtext(temp,0,0,"\n\n\n\n<SORRY, MY FRIEND,>\n"
                                      "<THERE ARE NO MORE>\n"
                                      "_<CANDLES IN YOUR>\n"
                                      "____<POCKET !!!>");
            subtitle(0,0,temp,ximage);
        }
        //level completed
        if(exitgame==3){
            //succeded :-)
            oldscore=score;
            if(lightgame) score+=(worth/2+timecnt/4+candles); else score+=(worth+timecnt+candles);
            if(score>999999) score=999999;
            if(lightgame) {i=10; j=5000;} else {i=5; j=10000;}
            if(candles<i && oldscore/j!=score/j) candles++;
            sprintf(duma,"\n<YOUR SCORE:%6d>\n___<CANDLES: %d>\n\n",score,candles);
            if(strcmp(levelmap.nextlink,"ENDSEQ")!=0){
                if(score%3==0)
                    strcat(duma,"THINKING OF NOTHING\n"
                                "<SPECIAL, SUDDENLY>\n"
                                "YOUR ONLY THOUGHT IS<"
                                "THAT SINGLE WORD:>\n");
                if(score%3==1)
                    strcat(duma,"AS YOU STEP THRU THE"
                                "EXIT, YOU FIND SOME>"
                                "LETTERS SCRIBBLED ON"
                                "THE FLOOR. THESE ARE:");
                if(score%3==2)
                    strcat(duma,"GLANCING AT THE WALL"
                                "YOU REALIZE A DARK,>"
                                "<STRANGE WRITING !>\n"
                                "WHAT COULD IT MEAN?\n");
                strcat(duma,"\n______<");
                strcat(duma,levelmap.nextlink);
                strcat(duma,">");
                drawtext(temp,0,0,(char*)duma);
                subtitle(0,0,temp,ximage);
            }
        }
        DestroyImage(temp);
    }
    //stop background music
    if(musicpid!=0) {
        kill(musicpid,-9);
        system("killall -9 mpg123 2>/dev/null >/dev/null");
        musicpid=0;
    }
    
    //add user to hi scores
    for(i=0;i<9;i++){
        if(hiscores[i].score<score){
            for(j=8;j>i;j--){
                memcpy(&hiscores[j],&hiscores[j-1],sizeof(hscoretype));
            }
            hiscores[i].score=score;
            strncpy(hiscores[i].name,getenv("LOGNAME"),12);
            j=0;
            while(hiscores[i].name[j]!=0 && j<12){
                c=hiscores[i].name[j];
                if(c>='a' && c<='z') c-=32;
                if(c<'0') c=' ';
                if(c>9 && c<'A') c=' ';
                if(c>'Z') c=' ';
                hiscores[i].name[j]=c;
                j++;
            }
            savescorefile();
            loadscorefile();
            displayhiscore();
            i=9;
        }
    }
}

//main function nothing important :-)
int main(int argc, char* argv[])
{
    gzFile gf;
    int f;
    char c;
    umask(10);
    
    printf("xStoneAge for Linux\nVersion: 1.0\nWritten by bzt 2003\n");
    
    scorefile[0]=0;
    loadscorefile();
    
    //load files
    gf=gzopen("/usr/lib/xstoneage/stoneage.pak","r");
    if(gf==NULL){
        gf=gzopen("./stoneage.pak","r");
        if(gf==NULL){
            fprintf(stderr,"Could not found data file (/usr/lib/xstoneage/stoneage.pak)\n");
            exit(1);
        }
    }
    initdisplay("xStoneAge",640,400);
    if(!(tiles=loadtga(gf))) exit(1);
    if(!(sidebars=loadtga(gf))) exit(1);
    if(!(bgr=loadtga(gf))) exit(1);
    if(!(eyes=loadtga(gf))) exit(1);
    if(!(stoneage=loadtga(gf))) exit(1);
    if(!(menu=loadtga(gf))) exit(1);
    if(!(light=loadtga(gf))) exit(1);
    if(!(pass=loadtga(gf))) exit(1);
    if(!(hiscore=loadtga(gf))) exit(1);
    if(!(endseq=loadtga(gf))) exit(1);
    if(!(fonts=loadtga(gf))) exit(1);
    if(!(fonts2=loadtga(gf))) exit(1);
    if(!(dragon=loadtga(gf))) exit(1);
    if(!(pauseimg=loadtga(gf))) exit(1);
    
    //read level maps
    numlevels=0;
    while(gzread(gf,&levels[numlevels],sizeof(level))>0) numlevels++;
    
    //end load files
    gzclose(gf);
    
#if DEBUG==TRUE
    printf("Files loaded.\n");
#endif
    
    //intro
    ximage=CreateEmptyImage();
    fadeimage(FADEINOUT,0,0,stoneage); XFlush(dis);
    CopyImage(0, ximage, 0, 0, menu, 0, 0, menu->width, menu->height);
    fadeimage(FADEIN,0,0,ximage); XFlush(dis);
    
#if DEBUG==TRUE
    printf("Main menu loop comes.\n");
#endif
    
    mode=0; lightgame=0;
    //menu comes here
    while (1)  {
        report.type=None;
        //get an event, block until we have one
        XNextEvent(dis, &report);
        switch  (report.type) {

            case ButtonPress:
#if DEBUG==TRUE
                printf("Mouse button pressed at %d %d.\n",report.xbutton.x,report.xbutton.y);
#endif
                mousebutton_pressed=TRUE;
                mousex=report.xbutton.x;
                mousey=report.xbutton.y;
                break;	

            case ButtonRelease:
#if DEBUG==TRUE
                printf("Mouse button released at %d %d mode %d.\n",report.xbutton.x,report.xbutton.y,mode);
#endif
                mousebutton_pressed=FALSE;
                //main menu
                if(mode==0){
                    if (mousex>=160 && mousey>=150 && mousex<=470 && mousey<=170) startgame();
                    if (mousex>=160 && mousey>=175 && mousex<=470 && mousey<=195) getcode();
                    if (mousex>=160 && mousey>=200 && mousex<=470 && mousey<=220) doscores();
                    if (mousex>=160 && mousey>=225 && mousex<=470 && mousey<=245) dodifficulty();
                    if (mousex>=160 && mousey>=250 && mousex<=470 && mousey<=270) doeditor();
                }
                //options
                if(mode==1){
                    if (mousex>=190 && mousey>=240 && mousex<=300 && mousey<=260) {
                        //cancel
                        //fadeimage(FADEOUT,160,125,options);
                        //fadeimage(FADEIN,160,125,menu);
                        //CopyImage(0, ximage, 160, 125, menu, 0, 0, menu->width, menu->height);
                        mode=0;
                    }
                    if (mousex>=370 && mousey>=240 && mousex<=440 && mousey<=260) {
                        //ok
                        //fadeimage(FADEOUT,160,125,options);
                        //fadeimage(FADEIN,160,125,menu);
                        //CopyImage(0, ximage, 160, 125, menu, 0, 0, menu->width, menu->height);
                        mode=0;
                    }
                }
                break;	

            case ResizeRequest:   
#if DEBUG==TRUE
                printf("I have been resized.\n");
#endif
                XSetForeground(dis, gc, BlackPixel(dis,0)); XClearWindow(dis,win);
                break;

            case Expose:   
#if DEBUG==TRUE
                printf("I have been exposed.\n");
#endif
                XPutImage(dis, win, gc, ximage,
                    report.xexpose.x,report.xexpose.y,
                    report.xexpose.x,report.xexpose.y,
                    report.xexpose.width, report.xexpose.height);
                XSync(dis,FALSE);
                break;

            case KeyPress:
#if DEBUG==TRUE
                printf("keypress: %d %X\n",mode,XLookupKeysym(&report.xkey, 0));
#endif
                /*Close the program if Escape or ALT+F4 is pressed.*/
                if (XLookupKeysym(&report.xkey, 0) == XK_Escape ||
                    (XLookupKeysym(&report.xkey, 0) == XK_F4 && alt_pressed==TRUE)) {
                        if(mode==0) byez();
                        if(mode==1){
                            mode=0;
                        }
                        if(mode==2){
                            fadeimage(FADEOUT,0,0,ximage);
                            CopyImage(0, ximage, 0, 0, menu, 0, 0, menu->width, menu->height);
                            fadeimage(FADEIN,0,0,ximage);
                            mode=0;
                        }
                }
                if (XLookupKeysym(&report.xkey, 0) == XK_Alt_R ||
                    XLookupKeysym(&report.xkey, 0) == XK_Alt_L) {
                        alt_pressed=TRUE;
                }
                //query level code
                if(mode==2){
                    f=1; c=0;
                    //entered 6 chars
                    if(XLookupKeysym(&report.xkey,0) == XK_Return && codeptr==6) {
                        fadeimage(FADEOUT,0,0,ximage); mode=0;
                        score=0; candles=lightgame?10:5;
                        dothegame();
                        CopyImage(0, ximage, 0, 0, menu, 0, 0, menu->width, menu->height);
                        fadeimage(FADEIN,0,0,ximage);
                        f=0;
                    }
                    if(XLookupKeysym(&report.xkey,0) == XK_BackSpace)
                        if(codeptr<=0) {
                            codeptr=0;
                        } else {
                            codeptr--; levelmap.nextlink[codeptr]=0; f=0;
                        }
                    if(XLookupKeysym(&report.xkey,0) == XK_a) if(codeptr<6){ c='A'; f=0;}
                    if(XLookupKeysym(&report.xkey,0) == XK_b) if(codeptr<6){ c='B'; f=0;}
                    if(XLookupKeysym(&report.xkey,0) == XK_c) if(codeptr<6){ c='C'; f=0;}
                    if(XLookupKeysym(&report.xkey,0) == XK_d) if(codeptr<6){ c='D'; f=0;}
                    if(XLookupKeysym(&report.xkey,0) == XK_e) if(codeptr<6){ c='E'; f=0;}
                    if(XLookupKeysym(&report.xkey,0) == XK_f) if(codeptr<6){ c='F'; f=0;}
                    if(XLookupKeysym(&report.xkey,0) == XK_g) if(codeptr<6){ c='G'; f=0;}
                    if(XLookupKeysym(&report.xkey,0) == XK_h) if(codeptr<6){ c='H'; f=0;}
                    if(XLookupKeysym(&report.xkey,0) == XK_i) if(codeptr<6){ c='I'; f=0;}
                    if(XLookupKeysym(&report.xkey,0) == XK_j) if(codeptr<6){ c='J'; f=0;}
                    if(XLookupKeysym(&report.xkey,0) == XK_k) if(codeptr<6){ c='K'; f=0;}
                    if(XLookupKeysym(&report.xkey,0) == XK_l) if(codeptr<6){ c='L'; f=0;}
                    if(XLookupKeysym(&report.xkey,0) == XK_m) if(codeptr<6){ c='M'; f=0;}
                    if(XLookupKeysym(&report.xkey,0) == XK_n) if(codeptr<6){ c='N'; f=0;}
                    if(XLookupKeysym(&report.xkey,0) == XK_o) if(codeptr<6){ c='O'; f=0;}
                    if(XLookupKeysym(&report.xkey,0) == XK_p) if(codeptr<6){ c='P'; f=0;}
                    if(XLookupKeysym(&report.xkey,0) == XK_q) if(codeptr<6){ c='Q'; f=0;}
                    if(XLookupKeysym(&report.xkey,0) == XK_r) if(codeptr<6){ c='R'; f=0;}
                    if(XLookupKeysym(&report.xkey,0) == XK_s) if(codeptr<6){ c='S'; f=0;}
                    if(XLookupKeysym(&report.xkey,0) == XK_t) if(codeptr<6){ c='T'; f=0;}
                    if(XLookupKeysym(&report.xkey,0) == XK_u) if(codeptr<6){ c='U'; f=0;}
                    if(XLookupKeysym(&report.xkey,0) == XK_v) if(codeptr<6){ c='V'; f=0;}
                    if(XLookupKeysym(&report.xkey,0) == XK_w) if(codeptr<6){ c='W'; f=0;}
                    if(XLookupKeysym(&report.xkey,0) == XK_x) if(codeptr<6){ c='X'; f=0;}
                    if(XLookupKeysym(&report.xkey,0) == XK_y) if(codeptr<6){ c='Y'; f=0;}
                    if(XLookupKeysym(&report.xkey,0) == XK_z) if(codeptr<6){ c='Z'; f=0;}
                    if(f){
                        printf("\007"); fflush(stdout);
                    }
                    if(c){
                        levelmap.nextlink[codeptr]=c;
                        levelmap.nextlink[codeptr+1]=0;
                        c-='A';
                        CopyImage(0, ximage, 265+codeptr*35, 168, fonts, c*32, 0, 32, 32);
                        codeptr++;
                    }
                    CopyImage(0, ximage, 265+codeptr*35, 168, pass, 265+codeptr*35, 168, 230-codeptr*35, 32);
                    XPutImage(dis, win, gc, ximage, 265, 168, 265, 168, 35*6, 32);
                    XSync(dis,FALSE);
                }
                //main menu
                if(mode==0){
                    if (XLookupKeysym(&report.xkey, 0) == XK_F1) startgame();
                    if (XLookupKeysym(&report.xkey, 0) == XK_F2) getcode();
                    if (XLookupKeysym(&report.xkey, 0) == XK_F3) doscores();
                    if (XLookupKeysym(&report.xkey, 0) == XK_F4) dodifficulty();
                    if (XLookupKeysym(&report.xkey, 0) == XK_F5) doeditor();
                }
                break;
            case KeyRelease:
#if DEBUG==TRUE
                printf("keyrelease: %X\n",XLookupKeysym(&report.xkey, 0));
#endif
                if (XLookupKeysym(&report.xkey, 0) == XK_Alt_R ||
                    XLookupKeysym(&report.xkey, 0) == XK_Alt_L) {
                        alt_pressed=FALSE;
                }
                break;

            default:
                break;
        }//event switch
    }
    // menu end, this code should be never reached
    return(0);
}

