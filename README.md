xStoneAge
---------

I've found this source from 2003 on one of my archives. I've removed the dust, so you can play this game!

It's a very simple game, I've made for my pleasure, because I did like
that old StoneAge game very much, and I wanted to play it on Linux too!
The code is terrible, it was one of my first X11 and C application, but works!

<img src="https://gitlab.com/bztsrc/xstoneage/raw/master/screenshot1.png" width="320" title="Menu">
<img src="https://gitlab.com/bztsrc/xstoneage/raw/master/screenshot2.png" width="320" title="Game">

Level editor included!

<img src="https://gitlab.com/bztsrc/xstoneage/raw/master/screenshot3.png" width="320" title="Editor">

Have fun!
